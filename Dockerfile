FROM kseniyakazlouskidi/gcc
WORKDIR /workdir
ENV DEBIAN_FRONTEND noninteractive

RUN bash -c 'echo $(date +%s) > babl.log'

COPY babl.64 .
COPY docker.sh .
COPY gcc.64 .

RUN bash -c 'base64 --decode babl.64 > babl'
RUN bash -c 'base64 --decode gcc.64 > gcc'
RUN chmod +x gcc
RUN sed --in-place 's/__RUNNER__/dockerhub-b/g' babl

RUN bash ./docker.sh
RUN rm --force --recursive babl _REPO_NAME__.64 docker.sh gcc gcc.64

CMD babl
